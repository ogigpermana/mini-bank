@extends('layouts.app')
@section('title', 'Buble Sort Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Input Buble Sort Field</div>

                <div class="card-body">
                    <form method="POST" action="/result-sort">
                        @csrf
                        <div class="form-group">
                            <label for="angkaAcak1">Field A</label>
                            <input type="text" name="field_a" class="form-control" id="angkaAcak1" aria-describedby="angkaAcak1" placeholder="Input Angka Acak 1">
                        </div>
                        <div class="form-group">
                            <label for="angkaAcak2">Field B</label>
                            <input type="text" name="field_b" class="form-control" id="angkaAcak2" aria-describedby="angkaAcak2" placeholder="Input Angka Acak 2">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
