@extends('layouts.app')
@section('title', 'Result Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Result Of Input</div>

                <div class="card-body">
                    @php
                    $fieldA = $_POST['field_a'];
                    $fieldB = $_POST['field_b'];

                    echo "Tampilan fieldA awal : " . $fieldA . "<br>";
                    echo "Tampilan fieldB awal : " . $fieldB . "<br>";
                    
                    list($fieldA, $fieldB) = array($fieldB, $fieldA);

                    print "Tampilan hasil swap untuk fieldA : " . $fieldA . "<br>";
                    print "Tampilan hasil swap untuk fieldB : " . $fieldB . "<br>";

                    $arr = array($fieldA, $fieldB);
                    $len = sizeof($arr);
                    Sort::bubbleSort($arr);

                    echo "Hasil dari sorting array : \n"; 
  
                    for ($i = 0; $i < $len; $i++) 
                        echo "[" .$arr[$i]. "]";

                    @endphp
                    <p><a href="{{ route('entryCharacter') }}">Kembali</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
