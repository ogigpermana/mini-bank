<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/find-duplicate-entry', 'DuplicateEntryController@index')->name('entryCharacter');
Route::post('/result', 'DuplicateEntryController@result');

Route::get('/input-buble-sort', 'BubleSortController@index')->name('entryAngka');
Route::post('/result-sort', 'BubleSortController@result');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
