@extends('layouts.app')
@section('title', 'Duplicate Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Displaying Repeated Of Letter</div>

                <div class="card-body">
                    <form method="POST" action="/result">
                        @csrf
                        <div class="form-group">
                            <label for="entryCharacter">Enter Character</label>
                            <input type="text" name="entryCharacter" class="form-control" id="entryCharacter" aria-describedby="entryCharacter" placeholder="Input Character">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
