@extends('layouts.app')
@section('title', 'Result Entry')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Result Of Input</div>

                <div class="card-body">
                    @php
                    $str = $_POST['entryCharacter'];
                    $post = Filter::cariKarakterUlang($str);

                    if ($post == -1)
                        echo 'Duplikasi Karakter Kosong';
                    else
                        echo ('Hasil dari filter karakter : ' . $str[$post]);
                    @endphp
                    <p><a href="{{ route('entryCharacter') }}">Kembali</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
