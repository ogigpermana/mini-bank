<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Sort;

class BubleSortController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index()
    {
        return view('bublesort.entry');
    }

    public function result()
    {
        return view('bublesort.result');
    }
}
