<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Helper untuk memproses filterisasi pada karakter yang dimasukan
use Filter;

class DuplicateEntryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    public function index()
    {
        // Fungsi untuk mengembalikan form inputan
        return view('filter.entry');
    }

    public function result()
    {
        // Fungsi untuk mengembalikan hasil dari inputan
        return view('filter.result');
    }
}
