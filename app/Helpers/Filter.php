<?php
namespace App\Helpers;

class Filter {
    public static function cariKarakterUlang($karakter)
    {
        // penggunaan O(N^2) method
        $pkarakter = -1;
        
        for ($i = 0; $i < strlen($karakter); $i++)
        {
            for ($j = ($i + 1); $j < strlen($karakter); $j++)
            {
                if ($karakter[$i] == $karakter[$j])
                {
                    $pkarakter = $i;
                    break;
                }
            }
            if ($pkarakter != -1)
            break;
        }
        return $pkarakter;
    }
}